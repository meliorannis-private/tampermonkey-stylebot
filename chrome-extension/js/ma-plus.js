$(function () {
    console.log("Savannah's MA Plus loaded");


    // filter above tables
    chrome.storage.sync.get("filter-table", result => {
        if (result['filter-table']) {

            new TableFilterBuilder('listings_table_targets', true).build();
            new TableFilterBuilder('listings_table_my_position', true).build();
            new TableFilterBuilder('listings_table_top20', true).build();
            new TableFilterBuilder('listings_table_by_glory', true).build();
            new TableFilterBuilder('listings_table_by_land', true).build();

            if (window.location.pathname === '/obchod.html') {

                new TableFilter('beast_shop', true, [{colIndex: 0, type: "text"}, {colIndex: 1, type: "select"},
                    {colIndex: 2, type: "number"}, {colIndex: 3, type: "number"}, {colIndex: 4, type: "number"},
                    {colIndex: 5, type: "number"}, {colIndex: 7, type: "number"}, {colIndex: 8, type: "select"},
                    {colIndex: 9, type: "select"}, {colIndex: 10, type: "select"}]);

                $('.block_row').hide();
            }
        }
    });

    // percentage per stack
    if (window.location.pathname === '/status.html') {
        const powerCols = $("#econ_unit_table").find("tbody > tr > td:nth-child(8)").get();
        const powers = powerCols.map(powerCol => Number($(powerCol).text()));

        if (powers.length !== 0) {
            const totalPower = powers.reduce((prev, cur) => prev + cur);

            powerCols.forEach((col, index) => {
                const percentage = ((powers[index] / totalPower) * 100).toFixed(1);
                $(col).append('<span style="color: green">  (' + percentage + '%)</span>');
            });
        }
    }

    // army to csv to clipboard + redirect btn
    if (window.location.pathname === '/status.html') {
        const csv = $("#econ_units_list").find("#econ_unit_table > tbody > tr:not('.tr_summary'):not('.tr_econ_summary')").get()
            .map(row => {
                const name = $(row).find("td:nth-child(1) > .tt_unit").get().map(col => textOfElementExcludeChildren($(col)))[0].trim();
                const exp = ($(row).children("td:nth-child(3)").get().map(col => Number($(col).text()))[0] / 100).toFixed(2);
                const count = $(row).children("td:nth-child(9)").get().map(col => Number($(col).text()))[0];

                return name + ";" + exp + ";" + count;
            })
			.join('\n');

        $("#econ_units_list").prepend('<a class="btn" style="width: 300px;" id="unitsCsvToClipboard">Zkopírovat armádu a simulovat souboj</a>');

        $("#unitsCsvToClipboard").click(() => {
            copyToClipboard(csv, () => {
                let win = window.open('http://sgm.meliorannis.com/tournaments/simul/', "_blank");
                win.focus();
            });
        });
    }

    if (window.location.pathname === '/obchod.html') {
        // remove 'copy to clip on click'. As that is a different scope event, we have to hack in it, see https://stackoverflow.com/a/40320909/600169
        let newScript = document.createElement('script');
        newScript.innerHTML = "$('#beast_shop td').off('click');";
        document.head.appendChild(newScript);

        function refresh_bestiary_icons() {
            $('i.fa-jedi').remove();

            let units = [];
            $('#best-rebels').val().split("\n").forEach(function (e) {
                if (e.trim() !== "") {
                    units.push(stripDiacriticsCase(e.trim()));
                }
            });

            $($('.beast_shop_unit_name').get().reverse()).each(function () {
                if (units.includes(stripDiacriticsCase($(this).find('.tt_unit').immediateText()))) {
                    $(this).parent('tr').prependTo($('#beast_shop'));
                    $('<i class="fas fa-jedi" style="color: #EEE133"></i> ').prependTo($(this));
                }
            });
        }

        $('<li style="background-color: #111516"><h4>Best Rebels</h4><textarea id="best-rebels" style="width: 160px; height: 190px"></textarea>').appendTo($('#status_wrapper').find('ul').last());


        chrome.storage.sync.get("best-rebels", result => {
            let data = '';
            if ('best-rebels' in result) {
                data = result['best-rebels'];
            } else {
                data = "Bazilišek\nGhoul\nLich King\nRudý drak\nSuccuba\nStřelec";
                chrome.storage.sync.set({"best-rebels": data});
            }

            $('#best-rebels')
                .val(data)
                .change(function () {
                    chrome.storage.sync.set({"best-rebels": $(this).val()});
                    refresh_bestiary_icons();
                });

            // let the callback chain hell begin!
            chrome.storage.sync.get("jmp-on-the-lookout", result => {
                if ("jmp-on-the-lookout" in result) {
                    let lookout_units = result["jmp-on-the-lookout"];

                    $('.beast_shop_unit_name').each(function () {
                        if (lookout_units.includes($(this).find('.tt_unit').immediateText())) {
                            $(this).parent('tr').prependTo($('#beast_shop'));
                            $('<i class="fas fa-running" style="color:#8080ff"></i> ').prependTo($(this));
                        }
                    });
                }

                refresh_bestiary_icons();
            });
        });

        chrome.storage.sync.get("jmp-selected", result => {
            if ("jmp-selected" in result) {
                $('<div></div>')
                    .text(result["jmp-selected"])
                    .prepend('<i class="fas fa-running" style="color:#8080ff"></i> ')
                    .append(' <i class="fas fa-running" style="color:#8080ff"></i>')
                    .insertBefore($('#best-rebels'));
            }
        });

        // buy it now buttons
        chrome.storage.sync.get("buy-it-now", result => {
            if (result['buy-it-now']) {
                $('#beast_shop > tbody > tr').each(function () {
                    let a = $(this).find('.beast_shop_quote a');

                    if (a.length !== 1) {
                        return;
                    }

                    let bid_id = a.attr('onclick').replace(/\D/g, '');

                    let form = $('<form method="post" action="obchod.html">');
                    $('<button name="best_nabidnout" value="Koupit" class="btn btn_offer_buy" style="padding-top: 0; padding-bottom: 0; margin-top: 0; margin-bottom: 0;" title="Buy it now!"><i class="fas fa-shopping-cart"></i></button>').appendTo(form);

                    let data = {};

                    $('form[name=formular] input').each(function () {
                        if (($(this).attr('type') === 'checkbox' || $(this).attr('type') === 'radio') && !$(this).is(':checked')) {
                            return;
                        }
                        if ($(this).attr('type'))
                            if ($(this).val()) {
                                data[$(this).attr('name')] = $(this).val();
                            }
                    });

                    data['nabidnout'] = bid_id;
                    data['nabidka_cena'] = parseInt(a.text());
                    delete data['best_nabidnout'];

                    for (let key in data) {
                        $('<input type="hidden">').attr('name', key).val(data[key]).appendTo(form);
                    }


                    form.appendTo($('<td>').appendTo($(this)));
                });
            }
        });
    }

    // load shield
    if (window.location.pathname === '/status.html') {
        chrome.storage.sync.get(x => console.log(x));
        let tr = $('#econ_unit_table tbody tr').first();
        if (tr.length === 1) {
            let name = tr.find('.econ_army_unit_name > .tt_unit').immediateText();
            let xp = parseFloat(tr.children('td:nth-child(3)').text());
            let pwr = parseInt(tr.children('td:nth-child(8)').text());
            let count = parseInt(tr.children('td:nth-child(9)').text());

            let shieldPwr = Math.round(pwr / count * 100) / 100;
            chrome.storage.sync.set({"spy-recruit-shield-pwr": shieldPwr});
            chrome.storage.sync.set({"spy-recruit-shield-total-pwr": pwr});
            chrome.storage.sync.set({"spy-recruit-shield-xp": xp});
            chrome.storage.sync.set({"spy-recruit-shield-name": name});
            chrome.storage.sync.set({"spy-recruit-shield-count": count});
        }
    }
    if (window.location.pathname === '/status.html') {
        let melee_units = [];
        let melee_units_by_name = {};
        let melee_units_by_id = {};
        all_units.forEach(unit => {
            if (!unit.shooter()) {
                melee_units.push(unit);
                melee_units_by_name[unit.name] = unit;
                melee_units_by_id[unit.id] = unit;
            }
        });
        melee_units.sort(Unit.compare);

        $.fn.immediateText = function () {
            return this.contents().not(this.children()).text().trim();
        };

        $('<li id="jmp-base" style="background-color: #111516"><h4><i class="fas fa-running" style="color:#8080ff"></i> Koho skákat <i class="fas fa-running" style="color:#8080ff"></i></h4>' +
            '<select style="width: 180px;" id="jmp-selected-unit"><option value="0">---</option></select>' +
            '<h4>Skokani <span id="jumpCount"></span></h4>' +
            '<table id="jmp-jumpers"></table></li>').appendTo($('#status_wrapper').find('ul').last());

        let owned_units = new Set();
        let owned_unit_count = [];
        let optgroup_own = $('<optgroup label="Vlastněné"></optgroup>').appendTo($('#jmp-selected-unit'));
        let optgroup_rest = $('<optgroup label="Ostatní"></optgroup>').appendTo($('#jmp-selected-unit'));

        $('#econ_unit_table > tbody > tr').each(function () {
            let td_unit_name = $(this).find('td[data-th="Jednotka"] > span.tt_unit');
            let unit_name = td_unit_name.immediateText();
            let unit = melee_units_by_name[unit_name];

            if (typeof unit !== 'undefined') {
                owned_units.add(unit_name);
                owned_unit_count[unit_name] = $(this).children('td:nth-child(9)').text();

                $('<option></option>')
                    .val(unit.id)
                    .text(unit.name + ' [' + unit.ini + '] ' + unit.hp_type())
                    .appendTo(optgroup_own);
            }
        });

        let melee_unit_names = Object.keys(melee_units_by_name);
        melee_unit_names.sort((a, b) => stripDiacriticsCase(a).localeCompare(stripDiacriticsCase(b)));

        melee_unit_names.forEach(unit_name => {
            if (owned_units.has(unit_name)) {
                return;
            }

            let unit = melee_units_by_name[unit_name];

            $('<option></option>')
                .val(unit.id)
                .text(unit.name + ' [' + unit.ini + '] ' + unit.hp_type())
                .appendTo(optgroup_rest);
        });

        melee_units.sort((a, b) => {
            const string_compare = stripDiacriticsCase(a.name).localeCompare(stripDiacriticsCase(b.name));
            const bool_compare = (+owned_units.has(b.name)) - (+owned_units.has(a.name));
            return bool_compare || string_compare;
        });

        melee_units.forEach(unit => {
            let tr = $('<tr style="border: none">')
                .attr('data-id', unit.id)
                .attr('data-ini', unit.ini)
                .attr('data-name', unit.name)
                .appendTo($('#jmp-jumpers'));

            if (owned_units.has(unit.name)) {
                $('<td><i class="fas fa-check-circle" style="color:#00c400"></i></td>').appendTo(tr);
                tr.data('missing', false);
            } else {
                $('<td><i class="fas fa-times-circle" style="color:red"></i></td>').appendTo(tr);
                tr.data('missing', true);
            }

            if (owned_unit_count[unit.name] !== undefined) {
                $('<td></td>').text(unit.name + ' (' + owned_unit_count[unit.name] + ')').appendTo(tr);
            } else {
                $('<td></td>').text(unit.name).appendTo(tr);
            }
            $('<td></td>').text(unit.hp_type()).addClass(unit.hp_class()).appendTo(tr);
            $('<td></td>').text(unit.ini).addClass(unit.ini_class()).appendTo(tr);
        });


        $('#jmp-selected-unit').change(function () {
            $('#jmp-jumpers > tr').show();

            let selected_id = parseInt($(this).val());
            let selected_unit = melee_units_by_id[selected_id];
            let on_the_lookout = [];

            chrome.storage.sync.set({"jmp-selected-id": selected_id});

            if (typeof selected_unit !== 'undefined') {
                chrome.storage.sync.set({"jmp-selected": selected_unit.summary()});
                $('#jmp-jumpers > tr').each(function () {
                    let tr = $(this);
                    if (tr.data('ini') > selected_unit.ini) {
                        tr.show();
                        if (tr.data('missing')) {
                            on_the_lookout.push(tr.data('name'));
                        }
                    } else {
                        tr.hide();
                    }
                });

                let jump_count = melee_units.filter(unit => {
                    return owned_units.has(unit.name) && unit.ini > melee_units_by_id[selected_id].ini;
                }).length;

                $('#jumpCount').html(jump_count);

            } else {
                chrome.storage.sync.remove("jmp-selected");
            }

            chrome.storage.sync.set({"jmp-on-the-lookout": on_the_lookout});
        });

        chrome.storage.sync.get("jmp-selected-id", result => {
            if (result["jmp-selected-id"]) {
                $('#jmp-selected-unit').val(result["jmp-selected-id"]).change();
            }
        });
    }

    // spies
    if (window.location.pathname === '/setup.html' && $('.spies_item:contains("Síla na útok:")').length === 1) {
        let sa = $('<div id="spy-attack">').appendTo($('#central_column_wrapper'));

        let table = $('<table>').css({'margin': '15px auto', 'width': '50%'}).appendTo(sa);
        $('<tr><td>Síla na útok chybí</td><td id="spy-attack-missing-pwr"></td><td></td><td></td></tr>').appendTo(table);
        $('<tr><td>Rekrut/TU</td><td><input type="text" id="spy-attack-recruit-per-tu"></td><td></td><td></td></tr>').appendTo(table);
        $('<tr><td>Kroutit TU</td><td id="spy-attack-recruit-tu"></td><td><span class="explore_wait_desktop text_left" id="spy-attack-recruit-tu-form-wrapper"></span></td><td></td></tr>').appendTo(table);
        $('<tr><td>Štít pwr/ks*</td><td><input type="text" id="spy-attack-shield-pwr"></td><td></td><td>*) automaticky se načte otevřením Hospodaření</td></tr>').appendTo(table);
        $('<tr><td>Štít</td><td></td><td id="spy-attack-shield-desc" colspan="2"></td></tr>').appendTo(table);
        $('<tr><td>Propustit štítu</td><td id="spy-attack-disband-shield"></td><td></td><td></td></tr>').appendTo(table);

        $('<tr><td id="spy-attack-disband-shield"></td></tr>').appendTo(table);
        let recruit_form = create_empty_form();
        recruit_form.attr('action', '/explore.html');
        recruit_form.appendTo($('#spy-attack-recruit-tu-form-wrapper'));
        let explore_button = $('<button class="btn btn_explore" type="submit" disabled>').text('Objevovat').appendTo(recruit_form);
        let explore_turns_input = $('<input type="text" name="kolikwait">').appendTo(recruit_form);

        let powerDiff = parseInt($('.spies_item:contains("Síla na útok:")').parent('tr').find('.spies_item_info').text()) - parseInt($('.value_power').text());

        chrome.storage.sync.get("spy-recruit-per-tu", result => {
            let val = result["spy-recruit-per-tu"] || 1000;
            $('#spy-attack-recruit-per-tu').val(val).change();
        });

        chrome.storage.sync.get(["spy-recruit-shield-pwr", "spy-recruit-shield-total-pwr", "spy-recruit-shield-xp", "spy-recruit-shield-name", "spy-recruit-shield-count"], result => {
            console.log(result);
            let val = result["spy-recruit-shield-pwr"] || 4.1;
            $('#spy-attack-shield-pwr').val(val).change();

            if (result['spy-recruit-shield-name']) {
                console.log(result);
                $('#spy-attack-shield-desc').text(nf_thousands(result["spy-recruit-shield-count"]) + 'x '
                    + result['spy-recruit-shield-name'] + ', '
                    + result['spy-recruit-shield-xp'] + '% ('
                    + nf_thousands(result['spy-recruit-shield-total-pwr']) + ' pwr)'
                );
            }
        });

        $('#spy-attack-missing-pwr').text(nf_thousands(powerDiff));

        $('#spy-attack-recruit-per-tu')
            .change(function () {
                let recruit_per_tu = parseInt($(this).val());

                if (powerDiff > 0) {
                    explore_turns_input.val(Math.floor(powerDiff / recruit_per_tu));
                    $('#spy-attack-recruit-tu').text(Math.round(powerDiff / recruit_per_tu * 10) / 10);
                } else {
                    explore_turns_input.val(0);
                    $('#spy-attack-recruit-tu').text('---');
                }

                // note the case where 0.9 still disables, 0 turns to explore
                explore_button.attr('disabled', powerDiff < recruit_per_tu);

                chrome.storage.sync.set({"spy-recruit-per-tu": recruit_per_tu});
            }).change();

        $('#spy-attack-shield-pwr')
            .change(function () {
                if (powerDiff < 0) {
                    $('#spy-attack-disband-shield').text(nf_thousands(Math.ceil(-powerDiff / $(this).val())));
                } else {
                    $('#spy-attack-disband-shield').text('---');
                }
                chrome.storage.sync.set({"spy-recruit-shield-pwr": $(this).val()});
            }).change();
    }

    if ((window.location.pathname === '/setup.html' || window.location.pathname === '/html/options.html') && $('input[name=email_hlidka]').length > 0) {
        let player_id = parseInt($('#player_regent > span.smaller').last().text().replace(/[^\d]/g, ''));
        let player_color = $($('#player_craftsmanship > span').get()[1]).text();
        let player_spec = $($('#player_craftsmanship > span').get()[2]).text();

        if (isNaN(player_id) || !player_color || !player_spec) {
            return;
        }

        chrome.storage.sync.set({
            'player_id': player_id,
            'player_color': player_color,
            'player_spec': player_spec,
        });
        let shortcuts_setup = $('<div id="shortcuts-setup"></div>')
            .appendTo($('#central_column_wrapper'));

        $('<h2>Nastavení filterování</h2>').appendTo(shortcuts_setup);
        $('<span class="setup_text"><label for="s-filter-table">Zapnout filterovaní v tabulkách:</label><input id="s-filter-table" class="s-filter-table" type="checkbox"></span>').appendTo(shortcuts_setup);
        $('#s-filter-table').change(function () {
            chrome.storage.sync.set({"filter-table": $(this).is(':checked')});
        });
        chrome.storage.sync.get("filter-table", result => {
            if ('filter-table' in result) {
                $('#s-filter-table').prop('checked', result['filter-table']);
            }
        });

        $('<h2>Nastavení objevovat</h2>').appendTo(shortcuts_setup);
        $('<span class="setup_text"><label for="s-right-explore">Výchozí nastavení Objevovat v pravém menu:</label><input id="s-right-explore" class="width_small s-target form-control form-control-sm" type="text"></span>').appendTo(shortcuts_setup);
        $('#s-right-explore').change(function () {
            chrome.storage.sync.set({"explore-tu": $(this).val()});
            $('#right_menu_wrapper #input_explore_wait').val($(this).val());
        });
        chrome.storage.sync.get("explore-tu", result => {
            if ('explore-tu' in result) {
                $('#s-right-explore').val(result['explore-tu']);
            }
        });

        $('<h2>Nastavení bestu</h2>').appendTo(shortcuts_setup);
        $('<span class="setup_text"><label for="s-best-buy-it-now">Zobrazit tlačítko Buy it now (může být pomalé):</label><input id="s-best-buy-it-now" class="s-best-buy-it-now" type="checkbox"></span>').appendTo(shortcuts_setup);
        $('#s-best-buy-it-now').change(function () {
            chrome.storage.sync.set({"buy-it-now": $(this).is(':checked')});
        });
        chrome.storage.sync.get("buy-it-now", result => {
            if ('buy-it-now' in result) {
                $('#s-best-buy-it-now').prop('checked', result['buy-it-now']);
            }
        });

        $('<h2>Nastavení buttonků</h2>').appendTo(shortcuts_setup);
        let table = $('<table></table>').appendTo(shortcuts_setup);
        $('<tr><th>Nadpis</th><th>Akce</th><th>Počet</th><th>Co</th><th>Na koho</th></tr>').appendTo(table);

        for (let i = 0; i < 10; ++i) {
            let row = $('<tr class="s-row">').appendTo(table);
            $('<td><input type="text" class="s-label form-control form-control-sm" value="" placeholder="Nadpis"></td>').appendTo(row);

            let select = $('<select class="s-action form-control form-control-sm">').appendTo($('<td>').appendTo(row));
            $('<option value="" selected>- akce tlačítka -</option>').appendTo(select);
            $('<option value="spell">Seslat kouzlo</option>').appendTo(select);
            $('<option value="recruit">Rekrutovat jednotku</option>').appendTo(select);
            $('<option value="cancel-recruit">Zrušit probíhající rekrutování</option>').appendTo(select);

            let count = $('<input type="text" class="s-count form-control form-control-sm" value="1">')
                .hide()
                .appendTo($('<td>').appendTo(row));

            let what_td = $('<td></td>').appendTo(row);
            let what_unit = $('<select class="s-what-unit form-control form-control-sm"></select>');
            $('<option value="0">- Rekrutovat vybranou jednotku -</option>').appendTo(what_unit);
            all_units.forEach(unit => {
                if (unit.color === player_color && unit.spec === player_spec) {
                    $('<option></option>')
                        .attr('value', unit.id)
                        .text(unit.name)
                        .appendTo(what_unit);
                }
            });
            what_unit
                .hide()
                .appendTo(what_td);

            let what_spell = $('<select class="s-what-spell form-control form-control-sm"></select>');
            $('<option value="">- Seslat vybrané kouzlo -</option>').appendTo(what_spell);
            ps_mo_spells.forEach(spell => {
                if (spell[2] === player_color && spell[3] === player_spec ||
                    spell[2] === '0' && spell[3] === '0') {
                    $('<option></option>')
                        .attr('value', spell[0])
                        .text(spell[1])
                        .appendTo(what_spell);
                }
            });
            what_spell
                .hide()
                .appendTo(what_td);


            let player_td = $('<td></td>');
            $('<input type="text" class="width_small s-target form-control form-control-sm">')
                .val(player_id)
                .hide()
                .appendTo(player_td);
            player_td
                .appendTo(row);
        }

        $('#shortcuts-setup .s-action').change(function () {
            let val = $(this).val();
            let row = $(this).parents('.s-row');

            let what_unit = row.find('.s-what-unit');
            let what_spell = row.find('.s-what-spell');
            let target = row.find('.s-target');
            let count = row.find('.s-count');

            what_unit.hide();
            what_spell.hide();
            target.hide();
            count.hide();

            if (val === "spell") {
                what_spell.show();
                target.show();
                count.show();
            } else if (val === "recruit") {
                what_unit.show();
                count.show();
            }
        });

        function any_input_changed() {
            store();
            create_custom_buttons();
        }

        function store() {
            let actions = [];
            $('#shortcuts-setup .s-row').each(function () {
                let row = $(this);

                let action = row.find('.s-action').val();
                let count = row.find('.s-count').val();
                let label = row.find('.s-label').val();

                if (!count) {
                    return;
                }

                if (action === 'spell') {
                    let spell_id = row.find('.s-what-spell').val();
                    let target = row.find('.s-target').val();

                    if (!spell_id || !target) {
                        return;
                    }

                    actions.push({
                        'type': 'spell',
                        'spell_id': spell_id,
                        'target': target,
                        'label': label,
                        'count': parseInt(count),
                    });
                } else if (action === 'recruit') {
                    let unit_id = row.find('.s-what-unit').val();

                    if (!unit_id) {
                        return;
                    }

                    actions.push({
                        'type': 'recruit',
                        'unit_id': unit_id,
                        'label': label,
                        'count': parseInt(count),
                    });
                } else if (action === 'cancel-recruit') {
                    actions.push({
                        'type': 'cancel-recruit',
                        'label': label,
                    });
                }
            });

            console.log('storing', actions);
            chrome.storage.sync.set({'shortcuts-setup': actions});
        }

        function load() {
            chrome.storage.sync.get('shortcuts-setup', result => {
                if ('shortcuts-setup' in result) {
                    let data = result['shortcuts-setup'];

                    data.forEach((action, i) => {
                        let row = $('#shortcuts-setup .s-row').eq(i);

                        let label = row.find('.s-label');
                        let what_unit = row.find('.s-what-unit');
                        let what_spell = row.find('.s-what-spell');
                        let target = row.find('.s-target');
                        let count = row.find('.s-count');

                        what_unit.hide();
                        what_spell.hide();
                        target.hide();
                        count.hide();

                        label.val(action['label']);

                        if (action['type'] === "spell") {
                            what_spell
                                .show()
                                .val(action['spell_id']);
                            target
                                .show()
                                .val(action['target']);
                            count
                                .show()
                                .val(action['count']);
                        } else if (action['type'] === "recruit") {
                            what_unit
                                .show()
                                .val(action['unit_id']);
                            count
                                .show()
                                .val(action['count']);
                        }

                        row.find('.s-action').val(action['type']);
                    });
                }
            });
        }

        load();

        $('#shortcuts-setup input').change(any_input_changed);
        $('#shortcuts-setup select').change(any_input_changed);
    }

    // army
    if (window.location.pathname === '/armada.html') {
        // click on a unit selects it
        $('#recruitables_table_army td[data-th=Jednotka]').click(function () {

            let unit_name = $(this).html();
            let select = $('form[name=rekrutovat] select[name=jednotka]');
            let val = select.find('option').filter(function () {
                return $(this).text().startsWith(unit_name)
            }).val();
            select.val(val);
            $('form[name=rekrutovat] input[name=kolik]').val('99999999').change();
        }).css('cursor', 'pointer');
    }

    chrome.storage.sync.get("explore-tu", result => {
        if ('explore-tu' in result) {
            $('#right_menu_wrapper #input_explore_wait').val(result['explore-tu']);
        }
    });

    create_custom_buttons();
});