class TableFilter {
    constructor(tableId, persistFilter, filterSettings) {
        this.table = $('#' + tableId);

        // if does not exist do nothing
        if (!this.table.length) {
            return;
        }

        this.tableId = tableId;
        this.filterSettings = filterSettings;
        this.filterId = tableId + 'tableFilter';
        this.filterData = [];
        this.rows = this.table.children('tbody').children('tr:not(".heading")').get();
        this.header = this.table.find('tr.heading');

        this.typeFunction = [];
        this.typeFunction['color'] = (td) => td.children("img").attr('src').replace(".png", "").replace("https://www.meliorannis.com/html/img/barvy/", "");
        this.typeFunction['text'] = (td) => td.text().trim();
        this.typeFunction['number'] = (td) => Number(td.text().trim().replace(" ", "").replace("%", ""));
        this.typeFunction['select'] = (td) => td.text().trim();
        this.typeFunction['militaryProt'] = (td) => td.children('.listings_prot_mil').length === 1;
        this.typeFunction['bot'] = (td) => td.text().trim().includes("[B] ");

        this._renderFilter();

        if (persistFilter) {
            this._loadPersistedFilterData(() => this._filterTable());
        }
    }

    _renderFilter() {

        // create filter table
        this.table.before('<div class="container table-filter"><div class="row" id="' + this.filterId + '"></div></div>');
        const filterRow = $("#" + this.filterId);

        // styles
        filterRow.append('<style>.btn:before { display: none;} .col-sm {border-radius: 0 !important; border: 0 !important; }</style>');

        // insert filter columns
        this.filterSettings.forEach(setting => {
            const filterFieldId = this.tableId + "filter" + setting.colIndex;
            const operatorFieldId = filterFieldId + 'operatorField';
            const filterName = $(this.header.children()[setting.colIndex]).text();

            switch (setting.type) {
                case "number":
                    this._appendNumberFilter(filterRow, filterName, operatorFieldId, filterFieldId);
                    break;
                case "text":
                    this._appendTextFilter(filterRow, filterName, filterFieldId);
                    break;
                case "select":
                    this._appendSelectFilter(filterRow, filterName, filterFieldId, setting, this.typeFunction[setting.type]);
                    break;
                case "color":
                    this._appendSelectFilter(filterRow, filterName, filterFieldId, setting, this.typeFunction[setting.type]);
                    break;
                case "militaryProt":
                    this._appendCheckboxFilter(filterRow,'Bez voj. prot.', filterFieldId);
                    break;
                case "bot":
                    this._appendCheckboxFilter(filterRow,'Bez botů', filterFieldId);
                    break;
            }

            const filterField = $("#" + filterFieldId);
            const operatorField = $("#" + operatorFieldId);

            const preventDefault = e => {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    return false;
                }
            };

            // stop propagation
            filterField.click(e => e.stopPropagation());
            operatorField.click(e => e.stopPropagation());
            filterField.keydown(preventDefault);
            operatorField.keydown(preventDefault);

            // listen to key changes
            filterField.keyup(e => {
                this._setFilterData(setting, filterField, operatorField);
                this._persisFilterData();
                this._filterTable();
            });

            filterField.change(e => {
                this._setFilterData(setting, filterField, operatorField);
                this._persisFilterData();
                this._filterTable();
            });
            operatorField.change(e => {
                this._setFilterData(setting, filterField, operatorField);
                this._persisFilterData();
                this._filterTable();
            });
        });

        const resetBtnId = this.tableId + 'resetBtn';
        filterRow.append('<div class="col-sm"><span id="' + resetBtnId + '" class="btn" style="height: 24px; margin-top: 16px;">Vyresetovat filter</span></div>');

        $('#' + resetBtnId).click(e => {
            this.filterData.forEach(fd => {
                if (fd) {
                    fd.value = "";
                    fd.operator = "<";
                }
            });
            this._setFilterFieldsValues();
            this._persisFilterData();
            this._filterTable();
        });
    }

    _filterTable() {
        this.rows.forEach(row => {
            const filterPass = this.filterSettings
                .map(setting => {
                    const filterData = this.filterData[setting.colIndex];
                    const colValue = this.typeFunction[setting.type]($($(row).children()[setting.colIndex]));

                    if (!filterData || filterData.value === "") {
                        return true;
                    }

                    switch (setting.type) {
                        case "number": {
                            const valNum = Number(filterData.value);

                            switch (filterData.operator) {
                                case "<":
                                    return colValue < valNum;
                                case ">":
                                    return colValue > valNum;
                                case "=":
                                    return colValue === valNum;
                            }
                            break;
                        }
                        case "text":
                            return colValue.toLocaleLowerCase().includes(filterData.value.toLowerCase());
                        case "select":
                            return colValue === filterData.value;
                        case "color": {
                            return colValue === filterData.value;
                        }
                        case "militaryProt": {
                            if (colValue && filterData.value) {
                                return false;
                            }
                            return true;
                        }
                        case "bot": {
                            if (colValue && filterData.value) {
                                return false;
                            }
                            return true;
                        }
                    }
                }).reduce((val1, val2) => val1 && val2);

            filterPass ? $(row).show() : $(row).hide();
        });
    }

    _setFilterData(filterSetting, filterField, operatorField) {
        let filterValue = filterField.val();

        if (filterField.attr('type') === 'checkbox') {
            filterValue = filterField.is(':checked');
        }

        this.filterData[filterSetting.colIndex] = {
            value: filterValue,
            operator: operatorField.val(),
            filterFieldId: filterField.attr('id'),
            operatorFieldId: operatorField.attr('id')
        };
    }

    _persisFilterData() {
        let keyVal = {};
        keyVal[this.tableId] = this.filterData;
        chrome.storage.sync.set(keyVal);
    }

    _loadPersistedFilterData(loadedCallback) {
        chrome.storage.sync.get(this.tableId, value => {
            const filterData = value[this.tableId];
            if (filterData) {
                this.filterData = filterData;
                this._setFilterFieldsValues();
                loadedCallback();
            }
        });
    }

    _setFilterFieldsValues() {
        this.filterData.forEach(fd => {
            if (fd && fd.filterFieldId) {
                const filterField = $('#' + fd.filterFieldId);

                if (filterField.attr('type') === 'checkbox') {
                    filterField.prop('checked', fd.value);
                }
                else {
                    filterField.val(fd.value);
                }
            }

            if (fd && fd.operatorFieldId) {
                $('#' + fd.operatorFieldId).val(fd.operator);
            }
        });
    }

    _appendCheckboxFilter(filterRow, filterName, filterFieldId) {
        filterRow.append(
            '<div class="col-sm" style="width: 90px; padding: 5px;">' +
            '<label style="display: block;">' + filterName + '</label>' +
            '<div style="display: block">' +
            '<input id="' + filterFieldId + '" type="checkbox">' +
            '</div>' +
            '</div>');
    }

    _appendSelectFilter(filterRow, filterName, filterFieldId, setting, dataSupplier) {
        let html = '<div class="col-sm" style="width: 150px; padding: 5px;">' +
            '<label style="display: block;">' + filterName + '</label>' +
            '<div style="display: block">' +
            '<select id="' + filterFieldId + '" style="width: 122px;">' +
            '<option></option>';

        this.rows.map(row => dataSupplier($($(row).children()[setting.colIndex])))
            .filter((value, index, self) => self.indexOf(value) === index)
            .forEach(colVal => html += '<option value="' + colVal + '">' + colVal + '</option>');

        html += "</select></div></div>";

        filterRow.append(html);
    }

    _appendTextFilter(filterRow, filterName, filterFieldId) {
        filterRow.append(
            '<div class="col-sm" style="width: 200px; padding: 5px;">' +
            '<label style="display: block;">' + filterName + '</label>' +
            '<div style="display: block">' +
            '<input id="' + filterFieldId + '" type="text" style="width: 150px; max-width: 150px;">' +
            '</div>' +
            '</div>');
    }

    _appendNumberFilter(filterRow, filterName, filterCriteriaId, filterFieldId) {
        filterRow.append(
            '<div class="col-sm" style="width: 150px; padding: 5px;">' +
            '<label style="display: block;">' + filterName + '</label>' +
            '<div style="display: block">' +
            '<select id="' + filterCriteriaId + '"><option value="<"><</option><option value="=">=</option><option value=">">></option></select>' +
            '<input id="' + filterFieldId + '" type="text" style="width: 50px; max-width: 50px;">' +
            '</div>' +
            '</div>');
    }
}

class TableFilterBuilder {
    constructor(tableId, persistFilter) {
        this.tableId = tableId;
        this.persistFilter = persistFilter;
        this.header = $('#' + tableId).find('tr.heading');
    }

    build() {
        const filterSettings = this.header.children('td').get().map((td, index) => {
            const colName = $(td).text().trim();

            switch (colName) {
                case "Regent" :
                    return {colIndex: index, type: "bot"};
                case "Síla provincie" :
                    return {colIndex: index, type: "number"};
                case "Síla" :
                    return {colIndex: index, type: "number"};
                case "Kvalita cíle" :
                    return {colIndex: index, type: "number"};
                case "Rozloha" :
                    return {colIndex: index, type: "number"};
                case "Aliance" :
                    return {colIndex: index, type: "select"};
                case "Sláva" :
                    return {colIndex: index, type: "number"};
                case "hAR" :
                    return {colIndex: index, type: "number"};
                case "Barva" :
                    return {colIndex: index, type: "color"};
                case "B." :
                    return {colIndex: index, type: "color"};
                case "Protekce":
                    return {colIndex: index, type: "militaryProt"};
            }

            return null;
        }).filter(val => !!val);

        return new TableFilter(this.tableId, this.persistFilter, filterSettings);
    }

}