chrome.storage.sync.get(result => {
    let player_color = result['player_color'];
    let player_id = result['player_id'];
    let player_spec = result['player_spec'];

    if (!player_color || !player_id || !player_spec) {
        $('#info-unknown-error').removeClass('d-none');
        return;
    }

    $('#player_color').text(player_color);
    $('#player_spec').text(player_spec);
    $('#player_id').text(player_id);
});
