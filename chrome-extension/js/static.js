class Unit {
    constructor(id, name, color, spec, attack, type, ini, speed) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.spec = spec;
        this.attack = attack;
        this.type = type;
        this.ini = ini;
        this.speed = speed;
    }

    shooter() {
        return this.attack === 'S';
    }

    flier() {
        return this.type === 'L';
    }

    hp_type() {
        if (this.shooter()) {
            return this.type + this.attack;
        }
        return this.type + this.attack + (this.speed > 1 ? this.speed : '');
    }

    hp_class() {
        if (this.flier()) {
            return 'color_unit_type_flying';
        }

        return 'color_unit_type_ground';
    }

    summary() {
        return this.name + ' ' + '[' + this.ini + '] ' + this.hp_type();
    }

    ini_class() {
        if (this.ini < 3) {
            return 'c702020';
        }
        if (this.ini < 7) {
            return 'c602020';
        }
        if (this.ini < 11) {
            return 'c502020';
        }
        if (this.ini < 15) {
            return 'c402020';
        }
        if (this.ini < 18) {
            return 'c302020';
        }
        if (this.ini < 22) {
            return 'c202020';
        }
        if (this.ini < 26) {
            return 'c203020';
        }
        if (this.ini < 30) {
            return 'c204020';
        }
        if (this.ini < 34) {
            return 'c205020';
        }
        if (this.ini <= 37) {
            return 'c206020';
        }
        return 'c207020';
    }

    /**
     * @param {Unit} a
     * @param {Unit} b
     */
    static compare(a, b) {
        if (a.speed !== b.speed) {
            return a.speed > b.speed ? -1 : 1;
        }

        if (a.ini !== b.ini) {
            return a.ini > b.ini ? -1 : 1;
        }

        return a.name.localeCompare(b.name);
    }
}

let all_units = [
    new Unit(1001, 'Kostlivec', 'C', 'M', 'B', 'P', 9, 1),
    new Unit(1002, 'Zombie', 'C', 'M', 'B', 'P', 5, 1),
    new Unit(1003, 'Stín', 'C', 'M', 'B', 'L', 14, 3),
    new Unit(1004, 'Upír', 'C', 'M', 'B', 'L', 19, 2),
    new Unit(1005, 'Fext', 'C', 'M', 'S', 'P', 29, 1),
    new Unit(1006, 'Spektra', 'C', 'M', 'B', 'L', 30, 2),
    new Unit(1007, 'Banshee', 'C', 'M', 'S', 'P', 25, 1),
    new Unit(1008, 'Mumie', 'C', 'M', 'B', 'P', 18, 1),
    new Unit(1009, 'Lich', 'C', 'M', 'S', 'L', 21, 1),
    new Unit(1013, 'Fantóm', 'C', 'B', 'B', 'P', 8, 1),
    new Unit(1014, 'Přízrak', 'C', 'B', 'S', 'L', 27, 1),
    new Unit(1016, 'Rarach', 'C', 'B', 'B', 'L', 27, 2),
    new Unit(1018, 'Děs', 'C', 'B', 'B', 'P', 20, 1),
    new Unit(1019, 'Ďábel', 'C', 'B', 'B', 'L', 33, 1),
    new Unit(1020, 'Noční můra', 'C', 'B', 'B', 'P', 14, 3),
    new Unit(1022, 'Hrůza', 'C', 'B', 'S', 'P', 24, 1),
    new Unit(1023, 'Tempest', 'C', 'B', 'S', 'L', 18, 1),
    new Unit(1024, 'Dračí démon', 'C', 'B', 'B', 'L', 36, 2),
    new Unit(2001, 'Štítonoš', 'B', 'M', 'B', 'P', 11, 1),
    new Unit(2002, 'Kopiník', 'B', 'M', 'B', 'P', 10, 1),
    new Unit(2003, 'Mnich', 'B', 'M', 'S', 'P', 27, 1),
    new Unit(2004, 'Křižák', 'B', 'M', 'B', 'P', 12, 2),
    new Unit(2005, 'Apoštol', 'B', 'M', 'B', 'L', 30, 2),
    new Unit(2006, 'Anděl', 'B', 'M', 'B', 'L', 25, 2),
    new Unit(2007, 'Zealot', 'B', 'M', 'S', 'P', 25, 1),
    new Unit(2008, 'Paladin', 'B', 'M', 'B', 'P', 28, 1),
    new Unit(2009, 'Archanděl', 'B', 'M', 'S', 'L', 20, 1),
    new Unit(2010, 'Pikenýr', 'B', 'B', 'B', 'P', 6, 1),
    new Unit(2011, 'Práče', 'B', 'B', 'S', 'P', 34, 1),
    new Unit(2012, 'Gryf', 'B', 'B', 'B', 'L', 17, 2),
    new Unit(2013, 'Dragoun', 'B', 'B', 'S', 'P', 27, 1),
    new Unit(2014, 'Husar', 'B', 'B', 'B', 'P', 30, 2),
    new Unit(2015, 'Bezhlavý rytíř', 'B', 'B', 'B', 'P', 21, 3),
    new Unit(2016, 'Bílý drak', 'B', 'B', 'S', 'L', 20, 1),
    new Unit(2017, 'Houfnice', 'B', 'B', 'S', 'P', 24, 1),
    new Unit(2018, 'Obrněná jízda', 'B', 'B', 'B', 'P', 25, 1),
    new Unit(3001, 'Satyr', 'Z', 'M', 'B', 'P', 7, 1),
    new Unit(3002, 'Víla', 'Z', 'M', 'S', 'L', 11, 1),
    new Unit(3003, 'Dryáda', 'Z', 'M', 'S', 'P', 26, 1),
    new Unit(3004, 'Pegas', 'Z', 'M', 'B', 'L', 23, 1),
    new Unit(3005, 'Kentaur', 'Z', 'M', 'B', 'P', 17, 2),
    new Unit(3006, 'Ohnivec', 'Z', 'M', 'B', 'L', 22, 2),
    new Unit(3007, 'Ent', 'Z', 'M', 'S', 'P', 22, 1),
    new Unit(3008, 'Jednorožec', 'Z', 'M', 'B', 'P', 29, 2),
    new Unit(3009, 'Fénix', 'Z', 'M', 'S', 'L', 18, 1),
    new Unit(3010, 'Vlk', 'Z', 'B', 'B', 'P', 11, 1),
    new Unit(3011, 'Duch stromu', 'Z', 'B', 'S', 'L', 23, 1),
    new Unit(3012, 'Elfí lučištník', 'Z', 'B', 'S', 'P', 30, 1),
    new Unit(3013, 'Medvěd', 'Z', 'B', 'B', 'P', 20, 1),
    new Unit(3014, 'Peryton', 'Z', 'B', 'B', 'L', 28, 1),
    new Unit(3015, 'Obří dikobraz', 'Z', 'B', 'S', 'P', 23, 1),
    new Unit(3016, 'Zelený drak', 'Z', 'B', 'B', 'L', 26, 1),
    new Unit(3017, 'Gargantua', 'Z', 'B', 'B', 'P', 31, 2),
    new Unit(3018, 'Zlatý drak', 'Z', 'B', 'S', 'L', 17, 1),
    new Unit(4001, 'Kobold', 'M', 'M', 'B', 'P', 7, 1),
    new Unit(4002, 'Mámení', 'M', 'M', 'B', 'L', 23, 1),
    new Unit(4003, 'Gargoyl', 'M', 'M', 'B', 'L', 12, 1),
    new Unit(4004, 'Kulový blesk', 'M', 'M', 'S', 'L', 28, 1),
    new Unit(4005, 'Železná panna', 'M', 'M', 'B', 'P', 15, 1),
    new Unit(4006, 'Čaroděj', 'M', 'M', 'S', 'P', 22, 1),
    new Unit(4007, 'Zlatá panna', 'M', 'M', 'B', 'P', 23, 1),
    new Unit(4008, 'Mystra', 'M', 'M', 'S', 'L', 16, 1),
    new Unit(4009, 'Titán', 'M', 'M', 'B', 'P', 32, 1),
    new Unit(4010, 'Dřevěný kolos', 'M', 'B', 'B', 'P', 6, 1),
    new Unit(4011, 'Meluzína', 'M', 'B', 'B', 'L', 16, 2),
    new Unit(4012, 'Kamenný obr', 'M', 'B', 'S', 'P', 31, 1),
    new Unit(4013, 'Homunkulus', 'M', 'B', 'B', 'L', 25, 1),
    new Unit(4014, 'Železný golem', 'M', 'B', 'B', 'P', 14, 1),
    new Unit(4015, 'Bouřkový obr', 'M', 'B', 'S', 'P', 24, 1),
    new Unit(4016, 'Gorgona', 'M', 'B', 'B', 'P', 21, 2),
    new Unit(4017, 'Mračný obr', 'M', 'B', 'S', 'L', 17, 1),
    new Unit(4018, 'Kyklop', 'M', 'B', 'B', 'P', 37, 1),
    new Unit(5001, 'Polární liška', 'S', 'B', 'B', 'P', 11, 1),
    new Unit(5002, 'Yeti', 'S', 'B', 'B', 'P', 9, 2),
    new Unit(5003, 'Valkýra', 'S', 'B', 'B', 'L', 24, 2),
    new Unit(5004, 'Sterling', 'S', 'B', 'S', 'P', 26, 1),
    new Unit(5005, 'Firbolg', 'S', 'B', 'B', 'P', 18, 1),
    new Unit(5006, 'Ledový dráček', 'S', 'B', 'S', 'L', 15, 1),
    new Unit(5007, 'Ledový obr', 'S', 'B', 'S', 'P', 25, 1),
    new Unit(5008, 'Remorhaz', 'S', 'B', 'B', 'P', 34, 1),
    new Unit(5009, 'Polární bouře', 'S', 'B', 'B', 'L', 37, 1),
    new Unit(5010, 'Gremlin', 'S', 'M', 'B', 'L', 9, 1),
    new Unit(5011, 'Vzdušný vír', 'S', 'M', 'S', 'L', 27, 1),
    new Unit(5012, 'Cockatrice', 'S', 'M', 'B', 'P', 18, 1),
    new Unit(5013, 'Wyverna', 'S', 'M', 'B', 'L', 15, 1),
    new Unit(5014, 'Naga', 'S', 'M', 'S', 'P', 22, 1),
    new Unit(5015, 'Behir', 'S', 'M', 'B', 'P', 26, 1),
    new Unit(5016, 'Písečný démon', 'S', 'M', 'S', 'L', 16, 1),
    new Unit(5017, 'Efreet', 'S', 'M', 'B', 'L', 29, 1),
    new Unit(5018, 'Arkana', 'S', 'M', 'S', 'P', 28, 1),
    new Unit(5501, 'Škorpion', 'F', 'M', 'B', 'P', 8, 1),
    new Unit(5502, 'Kobra', 'F', 'M', 'S', 'P', 20, 1),
    new Unit(5503, 'Chiméra', 'F', 'M', 'B', 'L', 32, 2),
    new Unit(5504, 'Harpyje', 'F', 'M', 'B', 'L', 28, 1),
    new Unit(5505, 'Siréna', 'F', 'M', 'S', 'P', 30, 1),
    new Unit(5506, 'Medúza', 'F', 'M', 'S', 'P', 35, 1),
    new Unit(5507, 'Gérion', 'F', 'M', 'B', 'P', 27, 1),
    new Unit(5508, 'Erínye', 'F', 'M', 'S', 'L', 22, 1),
    new Unit(5509, 'Týfón', 'F', 'M', 'B', 'P', 25, 2),
    new Unit(5510, 'Nymfa', 'F', 'B', 'S', 'P', 25, 1),
    new Unit(5511, 'Moira', 'F', 'B', 'S', 'P', 5, 1),
    new Unit(5512, 'Kerberos', 'F', 'B', 'B', 'P', 18, 2),
    new Unit(5513, 'Korybant', 'F', 'B', 'B', 'P', 29, 1),
    new Unit(5514, 'Falanga', 'F', 'B', 'B', 'P', 12, 1),
    new Unit(5515, 'Hoplit', 'F', 'B', 'B', 'P', 23, 1),
    new Unit(5516, 'Chariot', 'F', 'B', 'B', 'P', 26, 3),
    new Unit(5517, 'Balista', 'F', 'B', 'S', 'P', 22, 1),
    new Unit(5518, 'Sfinga', 'F', 'B', 'S', 'L', 7, 1),
    new Unit(6001, 'Bludička', 'N', 'N', 'B', 'L', 10, 2),
    new Unit(6002, 'Ork', 'N', 'N', 'B', 'P', 12, 1),
    new Unit(6003, 'Chalífa', 'N', 'N', 'B', 'L', 15, 1),
    new Unit(6004, 'Pixie', 'N', 'N', 'S', 'L', 11, 1),
    new Unit(6005, 'Obří orel', 'N', 'N', 'B', 'L', 19, 2),
    new Unit(6006, 'Válečný trpaslík', 'N', 'N', 'B', 'P', 19, 1),
    new Unit(6007, 'Striga', 'N', 'N', 'B', 'P', 18, 1),
    new Unit(6008, 'Zlovlk', 'N', 'N', 'B', 'P', 38, 2),
    new Unit(6009, 'Lev', 'N', 'N', 'B', 'P', 18, 2),
    new Unit(6010, 'Vlkodlak', 'N', 'N', 'B', 'P', 22, 1),
    new Unit(6011, 'Nomád', 'N', 'N', 'B', 'P', 31, 2),
    new Unit(6012, 'Succuba', 'N', 'N', 'S', 'P', 24, 1),
    new Unit(6013, 'Trol', 'N', 'N', 'B', 'P', 12, 2),
    new Unit(6014, 'Minotaur', 'N', 'N', 'B', 'P', 16, 1),
    new Unit(6015, 'Vozová hradba', 'N', 'N', 'S', 'P', 8, 1),
    new Unit(6016, 'Gladiátor', 'N', 'N', 'B', 'P', 16, 3),
    new Unit(6017, 'Mantikora', 'N', 'N', 'B', 'L', 12, 1),
    new Unit(6018, 'Rudý drak', 'N', 'N', 'S', 'L', 33, 1),
    new Unit(6019, 'Nemrtvý drak', 'N', 'N', 'B', 'L', 26, 2),
    new Unit(6020, 'Tarrasque', 'N', 'N', 'B', 'P', 17, 1),
    new Unit(6021, 'Bazilišek', 'N', 'N', 'S', 'P', 33, 1),
    new Unit(6022, 'Ghoul', 'N', 'N', 'B', 'P', 16, 1),
    new Unit(6023, 'Vodní drak', 'N', 'N', 'B', 'L', 14, 2),
    new Unit(6024, 'Válečný slon', 'N', 'N', 'S', 'P', 19, 1),
    new Unit(6025, 'Hydra', 'N', 'N', 'B', 'P', 21, 1),
    new Unit(6026, 'Lich King', 'N', 'N', 'S', 'L', 29, 1),
    new Unit(6027, 'Trebuchet', 'N', 'N', 'S', 'P', 36, 1),
    new Unit(6028, 'Bezejmenný', 'N', 'N', 'B', 'L', 10, 3),
    new Unit(6029, 'Hromový pták', 'N', 'N', 'B', 'L', 14, 1),
    new Unit(6030, 'Pěšák', 'N', 'N', 'B', 'P', 8, 1),
    new Unit(6031, 'Žoldák', 'N', 'N', 'B', 'P', 13, 1),
    new Unit(6032, 'Střelec', 'N', 'N', 'S', 'P', 28, 1),
    new Unit(6033, 'Lehká kavalérie', 'N', 'N', 'B', 'P', 21, 2),
    new Unit(6034, 'Gryfí jezdec', 'N', 'N', 'B', 'L', 37, 2),
    new Unit(6035, 'Mušketýr', 'N', 'N', 'S', 'P', 25, 1),
    new Unit(6036, 'Těžká kavalérie', 'N', 'N', 'B', 'P', 23, 1),
    new Unit(6037, 'Řádový rytíř', 'N', 'N', 'B', 'P', 29, 2),
    new Unit(6038, 'Revenant', 'N', 'N', 'S', 'L', 19, 1),
];

let ps_mo_spells = [
    [1008, 'Láska', 'B', 'M'],
    [1507, 'Tréninková aréna', 'B', 'B'],
    [2009, 'Populační exploze', 'C', 'M'],
    [2505, 'Elixír', 'C', 'B'],
    [3004, 'Zaklínání počasí', 'Z', 'M'],
    [3009, 'Harmonie', 'Z', 'M'],
    [3505, 'Klid a mír', 'Z', 'B'],
    [4007, 'Koncentrace', 'M', 'M'],
    [4009, 'Přání', 'M', 'M'],
    [4504, 'Poklad', 'M', 'B'],
    [5005, 'Blahobyt', 'S', 'M'],
    [5008, 'Štěstí', 'S', 'M'],
    [5503, 'Ódinův zpěv', 'S', 'B'],
    [6001, 'Ochrana před dobrem', '0', '0'],
    [6002, 'Ochrana před zemí', '0', '0'],
    [6003, 'Ochrana před myslí', '0', '0'],
    [6004, 'Ochrana před zlem', '0', '0'],
    [6006, 'Ochrana před větrem', '0', '0'],
    [6007, 'Ochrana před ženami', '0', '0'],
    [6005, 'Všechraň', '0', '0'],
    [7004, 'Transformace zlata', 'F', 'M'],
    [7005, 'Transformace many', 'F', 'M'],
    [7508, 'Podrobení', 'F', 'B'],
];
