$.fn.immediateText = function () {
    return this.contents().not(this.children()).text().trim();
};

function stripDiacriticsCase(s) {
    let diacritics = ["áa", "čc", "ďd", "ée", "ěe", "íi", "ňn", "óo", "řr", "šs", "ťt", "úu", "ůu", "ýy"];
    s = s.trim().toLowerCase();
    diacritics.forEach(function (d) {
        s = s.replace(new RegExp(d[0], 'g'), d[1]);
    });

    return s;
}

function nf_thousands(i) {
    return i.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

function create_empty_form() {
    let id = $('#search_wrapper input[name=id]').val();
    let code = $('#search_wrapper input[name=code]').val();
    let ftc = $('#search_wrapper input[name=ftc]').val();
    let form = $('<form method="post"></form>');
    $('<input type="hidden" name="id">')
        .val(id)
        .appendTo(form);
    $('<input type="hidden" name="code">')
        .val(code)
        .appendTo(form);
    $('<input type="hidden" name="ftc">')
        .val(ftc)
        .appendTo(form);
    return form;
}

// always load custom buttons
function create_custom_buttons() {
    $('#s-buttons').remove();
    let buttons_div = $('<div id="s-buttons" style="background-color: #111516"></div>')
        .appendTo('#right_column_wrapper');

    chrome.storage.sync.get("shortcuts-setup", result => {
        if (!("shortcuts-setup" in result)) {
            return;
        }

        let data = result['shortcuts-setup'];

        data.forEach(action => {
            let form = create_empty_form();

            $('<button class="btn" type="submit"></button>')
                .text(action['label'])
                .appendTo(form);

            if (action['type'] === "spell") {
                $('<input type="hidden" name="seslat_kouzlo">')
                    .val(action['spell_id'])
                    .appendTo(form);
                $('<input type="hidden" name="kolikrat">')
                    .val(action['count'])
                    .appendTo(form);
                $('<input type="hidden" name="koho">')
                    .val(action['target'])
                    .appendTo(form);
                form.attr('action', '/magie.html');
            } else if (action['type'] === "recruit") {
                $('<input type="hidden" name="jednotka">')
                    .val(action['unit_id'])
                    .appendTo(form);
                $('<input type="hidden" name="kolik">')
                    .val(action['count'])
                    .appendTo(form);
                form.attr('action', '/rekrutovat.html');
            } else if (action['type'] === "cancel-recruit") {
                $('<input type="hidden" name="jednotka">')
                    .val('jednotka=8859-2')
                    .appendTo(form);
                $('<input type="hidden" name="kolik">')
                    .val('0')
                    .appendTo(form);
                form.attr('action', '/rekrutovat.html');
            }

            form.appendTo(buttons_div);
        });

    });
}


function copyToClipboard(text, successCallback)
{
    var data = [new ClipboardItem({ "text/plain": new Blob([text], { type: "text/plain" }) })];
    navigator.clipboard.write(data).then(() =>
    {
        successCallback();
    });
}

function textOfElementExcludeChildren(element)
{
    return element.clone().children().remove().end().text();
}